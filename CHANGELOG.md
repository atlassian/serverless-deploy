# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.1.0

- minor: Upgrade pipe's base docker image to node:20-slim.
- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.
- patch: Internal maintenance: bump project's dependencies to the latest.
- patch: Update the Readme with details about The Serverless Framework version support. The pipe is based on The Serverless Framework V3 and will not be upgraded to V4 according to breaking changes in the new version.

## 2.0.0

- major: Breaking changes. Refactored pipe to support better auth logic and major providers AWS, GCP, AZURE.
- minor: Add OIDC support for AWS provider. Add support for AWS_OIDC_ROLE_ARN variable.
- minor: Add Poetry to the pipe's base docker image as an alternative tool for dependency management and packaging in Python.
- minor: Improved support for Azure cloud. Add serverless-azure-functions to the default environment.
- minor: Update base NodeJS version to nodejs20.
- minor: Update pipe's base docker image to python:3.11-slim.
- patch: Internal maintenance: bump project's dependencies to the latest.

## 1.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.4.1

- patch: Internal maintenance: Update README example.
- patch: Internal maintenance: Update docker pipes versions in pipelines configuration file.

## 1.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.3.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit version to 4.0.0.
- minor: Internal maintenance: Update docker image in Dockerfile to python:3.10-slim.
- minor: Update node version to 16.*.
- patch: Internal maintenance: Update docker image and pipes versions in pipelines configuration file.

## 1.2.0

- minor: Update serverless version to V3.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update modules in requirements.
- patch: Internal maintenance: update release process.

## 1.1.1

- patch: Update README doc with custom CONFIG example.

## 1.1.0

- minor: Support preexecution hook before executing pipe.

## 1.0.0

- major: Update NodeJS to 12 LTS version.
- major: Update the Serverless Framework to 2.* version.
- minor: Add support for Python: serverless-python-requirements, serverless-wsgi.

## 0.2.2

- patch: Internal maintenance: improve test infrastructure workflow.

## 0.2.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.1.6

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.1.5

- patch: Internal maintenance: Add gitignore secrets.

## 0.1.4

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.1.3

- patch: Internal maintenance: Add check for newer version.

## 0.1.2

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.1.1

- patch: Updated readme with GCP examples.

## 0.1.0

- minor: Initial release
- patch: Internal maintenance: update pipes toolkit version
