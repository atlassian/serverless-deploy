# Build stage
FROM node:20-slim AS build

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
    curl=7.*  \
    gnupg=2.* \
    python3-pip=23.* \
    && rm -rf /var/lib/apt/lists/*

# Install Serverless dependencies
RUN npm install -g serverless@3.* \
    && npm install --save -g serverless-google-cloudfunctions@4.* \
    && npm install --save -g serverless-deployment-bucket@1.* \
    && npm install --save -g serverless-azure-functions@v2.* \
    && npm install --save -g serverless-python-requirements@5.* \
    && npm install --save -g serverless-wsgi@3.* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install Poetry
RUN curl -sSL https://install.python-poetry.org | python3 - --version 1.8.3


# Final stage
FROM node:20-slim

# Copy installed Python packages and Poetry
COPY --from=build /root/.local /root/.local

# Copy Serverless dependencies
COPY --from=build /usr/local/bin /usr/local/bin
COPY --from=build /usr/local/lib/node_modules /usr/local/lib/node_modules

ENV PATH="${PATH}:/root/.local/bin:/usr/local/lib/node_modules/.bin:"

# Install system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
    python3-pip=23.* \
    && rm -rf /var/lib/apt/lists/*

# Set up Python environment
COPY requirements.txt /
RUN pip3 install --no-cache-dir --break-system-packages -r requirements.txt

# Copy application files
COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

WORKDIR /

ENTRYPOINT ["python3", "/main.py"]
