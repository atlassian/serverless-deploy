# Bitbucket Pipelines Pipe: Serverless deploy

Deploy a [Serverless][Serverless] application to AWS, Google Cloud, Microsoft Azure.


Note: The pipe is based on [The Serverless Framework V3][Serverless] and will not be upgraded to [V4 according to breaking changes in the new version][Serverless V4 breaking changes].


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/serverless-deploy:2.1.0
    variables:
      # CONFIG: "<string>" # Optional
      # EXTRA_ARGS: "<string>" # Optional
      # PRE_EXECUTION_SCRIPT: '<string>' # Optional.
      # DEBUG: "<boolean>" # Optional
```

## Variables

### Base variables

| Variable             | Usage                                                                                                                                |
|----------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| CONFIG               | Path to the Serverless framework configuration file. Default: `serverless.yml`.                                                      |
| EXTRA_ARGS           | Extra options to pass to the `serverless deploy` command as a string. These options depend on the deployment provider. Default: `''` |
| PRE_EXECUTION_SCRIPT | Path to pre-execution script to execute additional specific actions needed.                                                          |
| DEBUG                | Turn on extra debug information. Default: `false`.                                                                                   |


### Provider related variables

Deployment credentials are cloud provider specific.

| Variable              | Usage                                                                                                                                     |
|-----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID     | AWS key id. Default: `None`.                                                                                                              |
| AWS_SECRET_ACCESS_KEY | AWS secret key. Default: `None`.                                                                                                          |
| AWS_OIDC_ROLE_ARN     | The ARN of the role used for web identity federation or OIDC. See **AWS**.                                                                |
| GCP_KEY_FILE          | base64 encoded contents of the GCP key file. Default: `None`.                                                                             |
| SERVERLESS_ACCESS_KEY | Serverless access key for optional integration with the [Serverless Framework Dashboard][Serverless Framework Dashboard]. Default: `None` |
| AZURE_SUBSCRIPTION_ID | Azure subscription ID. Default: `None`.                                                                                                   |
| AZURE_TENANT_ID       | Azure service principal tenant ID. Default: `None`.                                                                                       |
| AZURE_CLIENT_ID       | Azure service principal client ID. Default: `None`.                                                                                       |
| AZURE_CLIENT_SECRET   | Azure service principal password. Default: `None`.                                                                                        |

_(*) = required variable._


## Prerequisites

* In order to use this pipe you need to create a Serverless application first.
* You will also need credentials for the cloud provider you will be deploying to.
* Check out the documentation for the cloud provider you are interested in the [official serverless documentation][official serverless documentation].


### *AWS*

Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.

2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you set up OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * set up a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on Lambda resources


### *Google Cloud Platform*

* Create a GCP project.
* Create an IAM member with at least a minimum set of roles: Cloud Functions Developer, Deployment Manager Editor, Storage Object Admin, and other needed for your resources.
* Create service account for your project.
* Create, download and save private_key.json. You will use it as your credentials in the `serverless.yml`.
* Enable the [Cloud Functions API][enable gcloud API].
* Enable the [Cloud Deployment Manager V2 API][enable gcloud API].


For base64 encoded contents required by GCP_KEY_FILE variable use command:

Linux
```
$ base64 -w 0 < GCP_key
```

MAC OS X
```
$ base64 < GCP_key
```


### *Microsoft Azure*

* Create a resource group, which is a logical container for related resources.
* Create a storage account, which maintains the state and other information about your projects.
* Create a function app, which provides the environment for executing your function code. A function app maps to your local function project and lets you group functions as a logical unit for easier management, deployment, and sharing of resources.
* Create an [Azure service principal][Azure service principal] and use credentials.



## Examples
### Basic examples

Deploying to AWS:

```yaml
script:
  - pipe: atlassian/serverless-deploy:2.1.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
```

Update the AWS lambda function with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:
```yaml

- step:
    oidc: true
    script:
      - pipe: atlassian/serverless-deploy:2.1.0
        variables:
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
```


Deploying to Google Cloud:

```yaml
script:
  - pipe: atlassian/serverless-deploy:2.1.0
    variables:
      GCP_KEY_FILE: $GCP_KEY_FILE
```


Deploying to Microsoft Azure:

```yaml
script:
  - pipe: atlassian/serverless-deploy:2.1.0
    variables:
      AZURE_SUBSCRIPTION_ID: $AZURE_SUBSCRIPTION_ID
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_CLIENT_ID: $AZURE_CLIENT_ID
      AZURE_CLIENT_SECRET: $AZURE_CLIENT_SECRET
```


### Advanced examples

Deploying with non-default config file `serverless.ts`:

```yaml
script:
  - pipe: atlassian/serverless-deploy:2.1.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      CONFIG: 'serverless.ts'
```


Deploying with extra args:

```yaml
script:
  - pipe: atlassian/serverless-deploy:2.1.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      EXTRA_ARGS: '--verbose'
```

Passing preexecution hook script file (file should have at least read and execute permissions) and deploying:

```yaml
script:
  - echo 'npm install serverless-additional@2.*' > .my-script.sh
  - chmod 005 .my-script.sh
  - pipe: atlassian/serverless-deploy:2.1.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      PRE_EXECUTION_SCRIPT: '.my-script.sh'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,serverless,lambda
[Serverless]: https://www.serverless.com/
[official serverless documentation]: https://serverless.com/framework/docs/
[Serverless Framework Dashboard]: https://www.serverless.com/framework/docs/guides/dashboard/cicd/running-in-your-own-cicd
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
[enable gcloud API]: https://cloud.google.com/endpoints/docs/openapi/enable-api
[Azure service principal]: https://learn.microsoft.com/en-us/cli/azure/azure-cli-sp-tutorial-1?tabs=bash
[Serverless V4 breaking changes]: https://www.serverless.com/framework/docs/guides/upgrading-v4#deprecation-of-non-aws-providers
