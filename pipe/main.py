import base64
import os
import sys
import subprocess

import boto3
import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'CONFIG': {'type': 'string', 'required': False, 'nullable': True, 'default': 'serverless.yml'},
    'EXTRA_ARGS': {'type': 'string', 'required': False, 'nullable': True, 'default': ''},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
    'GCP_KEY_FILE': {'type': 'string', 'required': False, 'nullable': True,
                     'excludes': ['AWS_OIDC_ROLE_ARN', 'AWS_ACCESS_KEY_ID', 'SERVERLESS_ACCESS_KEY', 'AZURE_CLIENT_ID']},
    'SERVERLESS_ACCESS_KEY': {'type': 'string', 'required': False, 'nullable': True,
                              'excludes': ['AWS_OIDC_ROLE_ARN', 'AWS_ACCESS_KEY_ID', 'GCP_KEY_FILE', 'AZURE_CLIENT_ID']},
    'AWS_OIDC_ROLE_ARN': {'type': 'string', 'required': False,
                          'excludes': ['GCP_KEY_FILE', 'AWS_ACCESS_KEY_ID', 'SERVERLESS_ACCESS_KEY', 'AZURE_CLIENT_ID']},
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': False, 'dependencies': ['AWS_SECRET_ACCESS_KEY'],
                          'excludes': ['SERVERLESS_ACCESS_KEY', 'AWS_OIDC_ROLE_ARN', 'GCP_KEY_FILE', 'AZURE_CLIENT_ID']},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': False, 'dependencies': ['AWS_ACCESS_KEY_ID']},
    'AZURE_SUBSCRIPTION_ID': {'type': 'string', 'required': False},
    'AZURE_TENANT_ID': {'type': 'string', 'required': False, 'dependencies': ['AZURE_CLIENT_ID', 'AZURE_CLIENT_SECRET']},
    'AZURE_CLIENT_ID': {'type': 'string', 'required': False, 'dependencies': ['AZURE_TENANT_ID', 'AZURE_CLIENT_SECRET'],
                        'excludes': ['SERVERLESS_ACCESS_KEY', 'AWS_ACCESS_KEY_ID', 'GCP_KEY_FILE', 'AWS_OIDC_ROLE_ARN']},
    'AZURE_CLIENT_SECRET': {'type': 'string', 'required': False, 'dependencies': ['AZURE_TENANT_ID', 'AZURE_CLIENT_ID']},
    'PRE_EXECUTION_SCRIPT': {'type': 'string', 'required': False}
}


class ServerlessDeploy(Pipe):
    AWS_OIDC_AUTH = 'AWS_OIDC_AUTH'
    AWS_DEFAULT_AUTH = 'AWS_DEFAULT_AUTH'
    GCP_KEYFILE_AUTH = 'GCP_KEYFILE_AUTH'
    AZURE_DEFAULT_AUTH = 'AZURE_DEFAULT_AUTH'
    SERVERLESS_DASHBOARD = 'SERVERLESS_DASHBOARD'

    def __init__(self, *args, **kwargs):
        self.aws_oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        self.web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        self.aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
        self.gcp_encoded_keyfile = os.getenv('GCP_KEY_FILE')
        self.azure_client_id = os.getenv('AZURE_CLIENT_ID')
        self.serverless_access_key = os.getenv('SERVERLESS_ACCESS_KEY')

        self.auth_method = self.discover_auth_method()
        super().__init__(*args, **kwargs)

        if self.auth_method is None:
            self.fail(f'There is no credentials discovered in the environment variables. '
                      f'Provide one of AWS, GCP, or AZURE. Follow the Readme {self.metadata["repository"]}')
        self.config = self.get_variable('CONFIG')

    def discover_auth_method(self):
        # AWS OIDC auth
        if self.aws_oidc_role:
            if not self.web_identity_token:
                logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')

            # update schema
            schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
            schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

            # unset env variables to prevent default auth with old creds
            if 'AWS_ACCESS_KEY_ID' in os.environ:
                del os.environ['AWS_ACCESS_KEY_ID']
            if 'AWS_SECRET_ACCESS_KEY' in os.environ:
                del os.environ['AWS_SECRET_ACCESS_KEY']
            if 'AWS_SESSION_TOKEN' in os.environ:
                del os.environ['AWS_SESSION_TOKEN']
            return self.AWS_OIDC_AUTH
        # AWS default auth
        elif self.aws_access_key_id:
            # update schema
            schema['AWS_ACCESS_KEY_ID']['required'] = True
            schema['AWS_SECRET_ACCESS_KEY']['required'] = True
            return self.AWS_DEFAULT_AUTH
        # GCP
        elif self.gcp_encoded_keyfile:
            # update schema
            schema['GCP_KEY_FILE']['required'] = True
            return self.GCP_KEYFILE_AUTH
        # AZURE
        elif self.azure_client_id:
            # update schema
            schema['AZURE_TENANT_ID']['required'] = True
            schema['AZURE_CLIENT_ID']['required'] = True
            schema['AZURE_CLIENT_SECRET']['required'] = True
            return self.AZURE_DEFAULT_AUTH
        elif self.serverless_access_key:
            # update schema
            schema['SERVERLESS_ACCESS_KEY']['required'] = True
            return self.SERVERLESS_DASHBOARD
        else:
            return None

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.AWS_OIDC_AUTH:
            self.log_info('Authenticating with an OpenID Connect (OIDC) Web Identity Provider')
            self.setup_aws_env_credentials()
        elif self.auth_method == self.AWS_DEFAULT_AUTH:
            self.log_info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
        elif self.auth_method == self.GCP_KEYFILE_AUTH:
            self.log_info('Using Google cloud default authentication with GCP_KEY_FILE.')
            self._write_gcp_keyfile(self.config)
        elif self.auth_method == self.AZURE_DEFAULT_AUTH:
            self.log_info('Using Microsoft Azure default authentication with AZURE_TENANT_ID, AZURE_CLIENT_ID and '
                          'AZURE_CLIENT_SECRET.')
        elif self.auth_method == self.SERVERLESS_DASHBOARD:
            self.log_info('Using Serverless Framework Dashboard authentication with SERVERLESS_ACCESS_KEY')

    def setup_aws_env_credentials(self):
        self.log_info("Assuming provided aws oidc role with the web-identity")

        client = boto3.client('sts')
        response = client.assume_role_with_web_identity(
            RoleArn=self.aws_oidc_role, RoleSessionName='current', WebIdentityToken=self.web_identity_token)

        self.log_info("Set up temporary AWS credentials to the environment")
        os.environ['AWS_ACCESS_KEY_ID'] = response['Credentials']['AccessKeyId']
        os.environ['AWS_SECRET_ACCESS_KEY'] = response['Credentials']['SecretAccessKey']
        os.environ['AWS_SESSION_TOKEN'] = response['Credentials']['SessionToken']

    def _write_gcp_keyfile(self, config):
        with open(config, 'r') as config_file:
            config_data = yaml.safe_load(config_file.read())
            location = config_data.get('provider', None).get('credentials', None)
        if location is None:
            self.log_warning('GCP key file path is not configured, can\'t write the contents of key file.')
        else:
            with open(location, 'w+') as key_file:
                key_file.write(base64.b64decode(self.gcp_encoded_keyfile).decode())

    def run_preexecution_script(self):
        preexecution_script = self.get_variable('PRE_EXECUTION_SCRIPT')

        if preexecution_script and not os.path.exists(preexecution_script):
            self.fail(f'Passed pre-execution script file {preexecution_script} does not exist.')
        if preexecution_script:
            try:
                self.log_info(f'Executing pre-execution script before pipe running sh {preexecution_script}')
                result = subprocess.run(['sh', preexecution_script], check=True, capture_output=True, text=True)
                if self.get_variable('DEBUG'):
                    self.log_debug(f'Executed pre-execution script. Output: {result.stdout}')
            except subprocess.CalledProcessError as exc:
                self.fail(f'{exc.output} {exc.stderr}')

    def run(self):
        self.auth()
        self.run_preexecution_script()

        self.log_info('Deploying your serverless application...')

        if not os.path.exists(self.config):
            self.fail(f'{self.config} doesn\'t exist.')

        extra_args = self.get_variable('EXTRA_ARGS')

        if self.get_variable('DEBUG'):
            os.environ['SLS_DEBUG'] = '*'
            verbose = '--verbose'
        else:
            verbose = ''

        cmd = f"serverless deploy -c {self.config} {extra_args} {verbose}"
        self.log_debug(cmd)

        result = subprocess.run(cmd.split(), stdout=sys.stdout, stderr=sys.stderr)

        if result.returncode != 0:
            self.fail(message="Deployment failed :(")

        self.success('Deployment completed successfully!')


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
        pipe = ServerlessDeploy(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
        pipe.run()
