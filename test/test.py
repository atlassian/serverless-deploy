import os
import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase


class DeployAWSServerlessDashboard(PipeTestCase):

    def setUp(self):
        os.chdir('test/aws-service-dashboard')

    def tearDown(self):
        os.chdir('../..')

    @pytest.mark.skip(reason="This test requires an AdministratorAccess"
                      "permissions to be granted to an external account")
    def test_build_successfully_started_serverless_key(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'SERVERLESS_ACCESS_KEY': os.getenv('SERVERLESS_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')


class DeployAWS(PipeTestCase):

    def setUp(self):
        os.chdir('test/aws-service')

    def tearDown(self):
        os.chdir('../..')

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deployment_successful_other_config(self):
        result = self.run_container(environment={
            'CONFIG': 'other-serverless.yml',
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deployment_fails_without_access_key_id(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        assert 'Validation errors' in result
        assert "field 'AWS_ACCESS_KEY_ID' is required" in result

    def test_deployment_fails_without_secret_access_key(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
        })

        assert 'Validation errors' in result
        assert "field 'AWS_SECRET_ACCESS_KEY' is required" in result

    def test_extra_logs_present_if_debug_with_preexecution_hook(self):
        preexecution = '.preexecution-hook.sh'
        with open(preexecution, 'w') as f:
            f.write('npm install serverless-http@3.*')
        os.chmod(preexecution, 0o0005)

        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'DEBUG': 'true',
            'PRE_EXECUTION_SCRIPT': preexecution
        })

        assert 'Executed pre-execution script' in result

    def test_extra_args(self):
        result = self.run_container(environment={
            'EXTRA_ARGS': '--verbose --force',
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        assert 'UPDATE_IN_PROGRESS' in result

    def test_pipe_fails_wrong_config_path(self):
        invalid_path = 'no-such-yaml.yml'
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'CONFIG': invalid_path
        })

        self.assertRegexpMatches(
            result, rf'{invalid_path} doesn\'t exist')

    def test_deploy_preexecution_hook_does_not_exist(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'PRE_EXECUTION_SCRIPT': '.does-not-exist.sh'
        })
        assert 'Passed pre-execution script file .does-not-exist.sh does not exist.' in result

    def test_deploy_preexecution_hook_failed(self):
        preexecution = '.preexecution-hook.sh'
        with open(preexecution, 'w') as f:
            f.write('npm install non-existing-package@2.*')
        os.chmod(preexecution, 0o0005)

        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'PRE_EXECUTION_SCRIPT': preexecution
        })
        assert "npm error 404  'non-existing-package@2.*' is not in this registry" in result

    def test_success_deployment_oidc_authentication(self):
        result = self.run_container(environment={
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
        })

        assert 'Authenticating with an OpenID Connect (OIDC) Web Identity Provider' in result
        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_fail_deployment_oidc_authentication_no_oidc_token(self):
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
        })

        assert 'Parameter "oidc: true" in the step configuration is required for OIDC authentication' in result


class DeployGCP(PipeTestCase):

    def setUp(self):
        os.chdir('test/gcp-service')

    def tearDown(self):
        os.chdir('../..')
        if os.path.exists('test/gcp-service/gcp-keyfile.json'):
            os.remove('test/gcp-service/gcp-keyfile.json')

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'GCP_KEY_FILE': os.getenv('GCP_KEY_FILE'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deployment_fails_without_keyfile(self):
        result = self.run_container(environment={})

        self.assertRegexpMatches(
            result, r'There is no credentials discovered in the environment variables')


class DeployAzure(PipeTestCase):

    def setUp(self):
        os.chdir('test/azure-service')

    def tearDown(self):
        os.chdir('../..')

    def test_fail_deployment_without_azure_principal_credentials(self):
        result = self.run_container(environment={
            'AZURE_CLIENT_ID': os.getenv('AZURE_CLIENT_ID'),
        })

        assert 'Validation errors' in result
        assert "field 'AZURE_TENANT_ID' is required" in result
